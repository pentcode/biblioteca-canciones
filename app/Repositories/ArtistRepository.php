<?php

namespace App\Repositories;

use App\Models\Artist;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ArtistRepository
 * @package App\Repositories
 * @version January 8, 2019, 7:22 pm UTC
 *
 * @method Artist findWithoutFail($id, $columns = ['*'])
 * @method Artist find($id, $columns = ['*'])
 * @method Artist first($columns = ['*'])
*/
class ArtistRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Artista',
        'Cancion',
        'song'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Artist::class;
    }
}
