<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Artist
 * @package App\Models
 * @version January 8, 2019, 7:22 pm UTC
 *
 * @property string Artista
 * @property string Cancion
 * @property string song
 */
class Artist extends Model
{
    use SoftDeletes;

    public $table = 'artists';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'Artista',
        'Cancion',
        'song'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Artista' => 'string',
        'Cancion' => 'string',
        'song' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Artista' => 'required',
        'Cancion' => 'required',
        'song' => 'required'
    ];

    
}
