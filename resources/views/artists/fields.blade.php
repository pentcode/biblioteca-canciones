<!-- Artista Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Artista', 'Artista:') !!}
    {!! Form::text('Artista', null, ['class' => 'form-control']) !!}
</div>

<!-- Cancion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('Cancion', 'Cancion:') !!}
    {!! Form::textarea('Cancion', null, ['class' => 'form-control']) !!}
</div>

<!-- Song Field -->
<div class="form-group col-sm-6">
    {!! Form::label('song', 'Song:') !!}
    {!! Form::file('song') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('artists.index') !!}" class="btn btn-default">Cancel</a>
</div>
