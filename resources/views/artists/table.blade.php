<table class="table table-responsive" id="artists-table">
    <thead>
        <tr>
            <th>Artista</th>
        <th>Cancion</th>
        <th>Song</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($artists as $artist)
        <tr>
            <td>{!! $artist->Artista !!}</td>
            <td>{!! $artist->Cancion !!}</td>
            <td>{!! $artist->song !!}</td>
            <!-- <td><img src="{!! $artist->song !!}" class="img-responsive" width="150" height="150"></td> -->
            <td>
                {!! Form::open(['route' => ['artists.destroy', $artist->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('artists.show', [$artist->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('artists.edit', [$artist->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>