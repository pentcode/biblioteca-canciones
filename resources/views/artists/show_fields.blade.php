<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $artist->id !!}</p>
</div>

<!-- Artista Field -->
<div class="form-group">
    {!! Form::label('Artista', 'Artista:') !!}
    <p>{!! $artist->Artista !!}</p>
</div>

<!-- Cancion Field -->
<div class="form-group">
    {!! Form::label('Cancion', 'Cancion:') !!}
    <p>{!! $artist->Cancion !!}</p>
</div>

<!-- Song Field -->
<div class="form-group">
    {!! Form::label('song', 'Song:') !!}
    <p>{!! $artist->song !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $artist->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $artist->updated_at !!}</p>
</div>

